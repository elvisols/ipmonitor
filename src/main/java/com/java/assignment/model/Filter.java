package com.java.assignment.model;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.java.assignment.util.RegexConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "filters")
@NamedQueries( {
	@NamedQuery(name = "Filter.findAll", query = "select f from Filter f order by f.id ")
})
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Filter {
	
	@SequenceGenerator(name="filter_id_gen",  sequenceName="filter_id_seq", initialValue = 2, allocationSize = 1)
	@Id	@GeneratedValue(generator="filter_id_gen")
	private Long id;
	
	@NotNull(message = "regex config parameter missing!")
	@Column(length = 255, unique = true, nullable = false)
	@RegexConstraint
	private String config;
	
	@Column(nullable = false)
    @CreationTimestamp
    private ZonedDateTime created;
	

}
