package com.java.assignment.util;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RegexValidator implements ConstraintValidator<RegexConstraint, String> {

  @Override
  public void initialize(RegexConstraint regex) {
  }

  @Override
  public boolean isValid(String regexField, ConstraintValidatorContext cxt) {
	  try {
          Pattern.compile(regexField);
      } catch (PatternSyntaxException exception) {
          return false;
      }
	  return true;
  }

}
