package com.java.assignment.controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.java.assignment.model.Blacklist;
import com.java.assignment.model.Filter;
import com.java.assignment.service.BlacklistService;
import com.java.assignment.service.FilterService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * A class to handle all API Blacklist requests.
 * 
 * <pre>
 *    Allowed Methods: GET
 * </pre>
 * 
 * @author  Elvis
 */
@Slf4j
@RestController
@RequestMapping("api/blacklists")
@Api(value = "Blacklist")
public class BlacklistController {

	@Autowired
	BlacklistService blacklistService;
	
	@Autowired
	FilterService filterService;
	
	private Pattern pattern;
    private Matcher matcher;
	
	/**
	* This method returns a true if an ip is blacklisted or false otherwise.
	*
	* @param <b>blacklistId</b> this is the ip to check.
	* @method GET
	* @return result, as boolean
	* @throws empty.
	*/
	@GetMapping(value="/{ip}", produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Check if an ip is blacklisted")
	public Boolean checkIp(@PathVariable String ip) {
		
		Blacklist blacklist = blacklistService.findBlacklistByIp(ip);
		log.info("blacklist returned = ", blacklist);
		
		if(blacklist == null) {
			return false;
			/*
			 *  Check if ip violates current config rule.
			 */
//			String current_config = "";
//			for(Filter f : filterService.findCurrentFilters()) {
//				current_config += f.getConfig() + "|";
//			}
//			 // check ip against current rule
//			this.pattern = Pattern.compile(current_config);
//			if(validate(ip)) {
//				// ip is blacklisted. Log it to blacklisted ip
//				blacklistService.save(new Blacklist(null, ip, null));
//			} else {
//				return false;
//			}
		}
		return true;
	}
	
	/**
    * Check ip address with regular expression
    * @param ip ip address for checking
    * @return true if match, false other
    */
    public boolean validate(final String ip){		  
	  matcher = pattern.matcher(ip);
	  return matcher.matches();	    	    
    }
	
}
