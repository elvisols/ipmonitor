package com.java.assignment.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.java.assignment.exception.ConstraintsViolationException;
import com.java.assignment.exception.FilterNotFoundException;
import com.java.assignment.model.Filter;
import com.java.assignment.service.FilterService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * A class to handle all API Filter requests.
 * This class exposes the Filter Config repository as REST.
 * 
 * <pre>
 *    Allowed Methods: GET, POST, DELETE
 * </pre>
 * 
 * @author  Elvis
 */
@Slf4j
@RestController
@RequestMapping("api/filters")
@Api(value = "Filter")
public class FilterController {
	
	@Autowired
	FilterService filterService;
	
	/**
	* This method fetches the current filters
	*
	* @param void
	* @method GET
	* @return result as list of filter object
	*/
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Current Filters")
	public List<Filter> findFilters() {
		return filterService.findCurrentFilters();
	}
	
	/**
	* This method returns a newly created filter record.
	*
	* @param <b>filterDO</b> this is the JSON filter object to create.
	* @method POST
	* @return result, as a filter object
	* @throws ConstraintsViolationException, if JSON filter object violates some constraints.
	*/
	@PostMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Create a filter")
	public Filter addFilter(@Valid @RequestBody Filter filter, Errors errors) throws ConstraintsViolationException {
		if (errors.hasErrors()) {
			log.error("Error in transaction body detected...{}", errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(", ")));
			throw new ConstraintsViolationException(errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(", ")));
		}
		return filterService.createFilter(filter);
	}
	
	/**
	* This method "hard" deletes a single filter record.
	*
	* @param <b>filterId</b> this is the id of the filter record to delete.
	* @method DELETE
	* @return void
	* @throws FilterNotFoundException if filter record for the specified id cannot be retrieved.
	*/
	@DeleteMapping(value="/{filterId}", produces=MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Delete a filter with a particular id")
	public ResponseEntity<Void> deleteFilter(@Valid @PathVariable long filterId) throws FilterNotFoundException {
		filterService.deleteFilter(filterId);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
