package com.java.assignment.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.java.assignment.model.Blacklist;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class BlacklistRepository {

	@Autowired
	private EntityManager em;
	
	public Blacklist findByIp(String ip) {
		log.info("Getting Blacklisted ip...");
		TypedQuery<Blacklist> customQuery = em.createNamedQuery("Blacklist.findByIp", Blacklist.class)
				.setParameter("ip", ip);
		log.info("returning... {}", customQuery);
		
		try {
			return customQuery.getSingleResult();
		} catch(Exception e) {
			return null;
		}
	}
	
	public Blacklist save(Blacklist blacklist) {

		if (blacklist.getId() == null) {
			em.persist(blacklist);
		} else {
			em.merge(blacklist);
		}

		return blacklist;
	}
	
}
