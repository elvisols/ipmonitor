package com.java.assignment.repository;

import org.springframework.data.repository.CrudRepository;

import com.java.assignment.model.Filter;

public interface FilterRepository extends CrudRepository<Filter, Long> {

}
