package com.java.assignment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.assignment.model.Blacklist;
import com.java.assignment.repository.BlacklistRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BlacklistImpl implements BlacklistService {

	@Autowired
    private BlacklistRepository blacklistRepository; 
	
	@Override
	public Blacklist findBlacklistByIp(String ip) {
		log.info("...Getting blacklist with Ip: ", ip);
		return blacklistRepository.findByIp(ip);
	}
	
	@Override
	public Blacklist save(Blacklist blacklist) {
		log.info("...Saving blacklist: ", blacklist);
		return blacklistRepository.save(blacklist);
	}

}
