package com.java.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.assignment.exception.ConstraintsViolationException;
import com.java.assignment.exception.FilterNotFoundException;
import com.java.assignment.model.Filter;
import com.java.assignment.repository.FilterRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FilterImpl implements FilterService {

	@Autowired
    private FilterRepository filterRepository; 
	
	@Override
	public Filter createFilter(Filter filter) throws ConstraintsViolationException {
		log.info("...Saving filter configuration: ", filter);
		return filterRepository.save(filter);
	}

	@Override
	public void deleteFilter(Long filterId) throws FilterNotFoundException {
		log.warn("...Deleting filter config with Id: ", filterId);
		filterRepository.deleteById(filterId);
	}

	@Override
	public List<Filter> findCurrentFilters() {
		log.info("...Fetching current filters");
		return (List<Filter>) filterRepository.findAll();
	}

}
