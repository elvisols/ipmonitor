package com.java.assignment.service;

import java.util.List;

import com.java.assignment.exception.ConstraintsViolationException;
import com.java.assignment.exception.FilterNotFoundException;
import com.java.assignment.model.Filter;

public interface FilterService {

	Filter createFilter(Filter filter) throws ConstraintsViolationException;
    
    void deleteFilter(Long filterId) throws FilterNotFoundException;
    
    List<Filter> findCurrentFilters();
    
}
