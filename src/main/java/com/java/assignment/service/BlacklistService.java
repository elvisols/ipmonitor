package com.java.assignment.service;

import com.java.assignment.model.Blacklist;

public interface BlacklistService {

	Blacklist findBlacklistByIp(String ip);
	
	Blacklist save(Blacklist blacklist);
	
}
