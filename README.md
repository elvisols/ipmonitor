##	IP Monitoring Code challenge

A Java application with the following REST endpoints:
● Add a filtering configuration to the blacklist. 
● Delete a filtering configuration from the blacklist
● Get current blacklist configuration
● Check if an IP address is present in the blacklist

- **Configurations are regex based configurations**
- All endpoints are exposed on (**Port: 8181**).


######Application Requirements:
- Java 8
- Maven 3+

######Deploying and Running application:
> This should be done from the root directory
```
~$ mvn clean package spring-boot:run
```
Once the service is up, the application can be accessed on port 8181.

Documentation can be found here http://localhost:8181/swagger-ui.html

				
##   Cheers! 

